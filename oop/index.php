<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>"; // "buduk"
echo "Legs : $kodok->legs <br>"; // 4
echo "cold blooded : $kodok->cold_blooded <br>"; // "no"
echo "Jump : "; 
echo $kodok->jump()."<br><br>"; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>"; // "kera sakti"
echo "Legs : $sungokong->legs <br>"; // 2
echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
echo "Yell : ";
echo $sungokong->yell(); // "Auooo"
